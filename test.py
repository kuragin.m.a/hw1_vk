import pytest
import pandas as pd
from functions import drop_columns
from functions import remane_columns
from functions import rename_values
from functions import classify_age

def test_drop_columns(df):
    result_df = drop_columns(df)
    expected_columns =  ['id', 'bdate', 'followers_count', 'university_name', 
                         'faculty_name', 'graduation', 'home_town', 'relation', 'sex', 
                         'first_name', 'last_name', 'is_closed', 'education_form', 'education_status']
    assert list(result_df.columns) == expected_columns

def test_rename_columns(df):
    result_df = remane_columns(df)
    expected_columns = ['id', 'Имя', 'Фамилия', 'Пол', 'Дата рождения', 'Город рождения',
                         'Семейное положение', 'Университет', 'Факультет', 'Тип обучения',
                         'Статус обучения', 'Год окончания университета', 'Кол-во подписчиков',
                         'Скрытый профиль']
    assert list(result_df.columns) == expected_columns

def test_rename_values(df):
    result_df = rename_values(df)
    assert result_df['Пол'].isin(['Женский', 'Мужской']).all()
    assert result_df['Скрытый профиль'].isin(['Скрытый', 'Не скрытый']).all()

def test_classify_age(df):
    result_df = classify_age(df)
    assert 'Возраст' in result_df.columns
    assert 'Классификация возраста' in result_df.columns
